/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.19-MariaDB : Database - absensi_operator
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`absensi_operator` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `absensi_operator`;

/*Table structure for table `log_absensi` */

DROP TABLE IF EXISTS `log_absensi`;

CREATE TABLE `log_absensi` (
  `id_log` int(10) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `id_operator` int(10) NOT NULL,
  `nama_operator` varchar(50) DEFAULT NULL,
  `tempat_operator` varchar(50) DEFAULT NULL,
  `status_absen` varchar(10) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `log_absensi` */

insert  into `log_absensi`(`id_log`,`tanggal`,`id_operator`,`nama_operator`,`tempat_operator`,`status_absen`,`keterangan`) values (1,'2018-12-07 07:58:45',1,'Ahmad Zaky Maula','Bandar Kidul','M','Masuk'),(2,'2018-12-07 16:59:58',1,'Ahmad Zaky Maula','Bandar Kidul','P','Pulang'),(3,'2018-12-07 07:54:34',2,'Tahajud','Tinalan','M','Masuk'),(4,'2018-12-07 09:19:08',4,'Zaky','Bandar Lor','MT','Masuk Telat'),(5,'2018-12-07 16:22:59',4,'Zaky','Bandar Lor','P','Pulang'),(9,'2018-12-08 07:46:23',3,'Ahmad','Lirboyo','M','Masuk'),(10,'2018-12-10 10:55:47',1,'Ahmad Zaky Maula','Bandar Kidul','MT','Masuk Telat');

/*Table structure for table `operator` */

DROP TABLE IF EXISTS `operator`;

CREATE TABLE `operator` (
  `id_operator` int(10) NOT NULL AUTO_INCREMENT,
  `imei` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat_operator` varchar(30) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `norekening` varchar(30) NOT NULL,
  PRIMARY KEY (`id_operator`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `operator` */

insert  into `operator`(`id_operator`,`imei`,`nama`,`tempat_operator`,`nohp`,`norekening`) values (1,'357887069989696','Ahmad Zaky Maula','Bandar Kidul','085745170255','123123123123123'),(2,'867796035708325','Tahajud','Tinalan','085085085085','123123123123123'),(3,'123','Ahmad','Lirboyo','123','123'),(4,'123','Zaky','Bandar Lor','123','123'),(5,'123','Maula','Banjarmlati','123','123'),(6,'123','Taha','Pesantren','123','123'),(7,'123','Juda','Balowerti','123','123');

/*Table structure for table `status_absensi` */

DROP TABLE IF EXISTS `status_absensi`;

CREATE TABLE `status_absensi` (
  `code_status` char(5) NOT NULL,
  `nama_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`code_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `status_absensi` */

insert  into `status_absensi`(`code_status`,`nama_status`) values ('M','Masuk'),('MT','Masuk Telat'),('P','Pulang');

/*Table structure for table `waktu_absen` */

DROP TABLE IF EXISTS `waktu_absen`;

CREATE TABLE `waktu_absen` (
  `masuk_start` time DEFAULT NULL,
  `masuk_stop` time DEFAULT NULL,
  `pulang_start` time DEFAULT NULL,
  `pulang_stop` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `waktu_absen` */

insert  into `waktu_absen`(`masuk_start`,`masuk_stop`,`pulang_start`,`pulang_stop`) values ('07:00:00','08:00:00','16:00:00','17:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
