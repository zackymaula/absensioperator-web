/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.19-MariaDB : Database - absensi_operator
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `usernm` varchar(50) DEFAULT NULL,
  `passwd` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id_admin`,`nama`,`usernm`,`passwd`) values (1,'Admin Absensi','admin','123456');

/*Table structure for table `log_absensi` */

DROP TABLE IF EXISTS `log_absensi`;

CREATE TABLE `log_absensi` (
  `id_log` int(10) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `id_operator` int(10) NOT NULL,
  `nama_operator` varchar(50) DEFAULT NULL,
  `tempat_operator` varchar(50) DEFAULT NULL,
  `status_absen` varchar(10) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `log_absensi` */

insert  into `log_absensi`(`id_log`,`tanggal`,`id_operator`,`nama_operator`,`tempat_operator`,`status_absen`,`keterangan`) values (2,'2018-12-07 16:59:58',1,'Ahmad Zaky Maula','Bandar Kidul','P','Pulang'),(3,'2018-12-07 07:54:34',2,'Tahajud Mandariansyah','Tinalan','M','Masuk'),(10,'2018-12-10 10:55:47',1,'Ahmad Zaky Maula','Bandar Kidul','MT','Masuk Telat'),(22,'2018-12-16 19:55:08',1,'Ahmad Zaky Maula','Bandar Kidul','P','Pulang'),(27,'2018-12-16 20:11:58',1,'Ahmad Zaky Maula','Bandar Kidul','MT','Masuk Telat');

/*Table structure for table `operator` */

DROP TABLE IF EXISTS `operator`;

CREATE TABLE `operator` (
  `id_operator` int(10) NOT NULL AUTO_INCREMENT,
  `imei` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat_operator` varchar(30) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `norekening` varchar(30) NOT NULL,
  PRIMARY KEY (`id_operator`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `operator` */

insert  into `operator`(`id_operator`,`imei`,`nama`,`tempat_operator`,`nohp`,`norekening`) values (1,'357887069989696','Ahmad Zaky Maula','Bandar Kidul','085745170255','123123123123123'),(2,'867796035708325','Tahajuda Mandariansyah','Tinalan','085085085085','123123123123123');

/*Table structure for table `status_absensi` */

DROP TABLE IF EXISTS `status_absensi`;

CREATE TABLE `status_absensi` (
  `code_status` char(5) NOT NULL,
  `nama_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`code_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `status_absensi` */

insert  into `status_absensi`(`code_status`,`nama_status`) values ('M','Masuk'),('MT','Masuk Telat'),('P','Pulang');

/*Table structure for table `waktu_absen` */

DROP TABLE IF EXISTS `waktu_absen`;

CREATE TABLE `waktu_absen` (
  `id_waktu` int(11) NOT NULL,
  `masuk_start` time DEFAULT NULL,
  `masuk_stop` time DEFAULT NULL,
  `pulang_start` time DEFAULT NULL,
  `pulang_stop` time DEFAULT NULL,
  PRIMARY KEY (`id_waktu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `waktu_absen` */

insert  into `waktu_absen`(`id_waktu`,`masuk_start`,`masuk_stop`,`pulang_start`,`pulang_stop`) values (1,'07:00:00','08:00:00','15:30:00','16:30:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
