<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operator extends CI_Controller {
    
	public function index()
	{
        $this->load->view('header');
        $data = $this->operator_m->GetData();
		$this->load->view('operator_v',array('data_operator' => $data));
        $this->load->view('footer');
	}

	public function insert()
	{
		$this->load->view('header');
		$this->load->view('operator_insert_v');
        $this->load->view('footer');
	}

	public function do_insert()
	{
		$imei = $_POST['post_imei'];
		$nama = $_POST['post_nama'];
		$kelurahan = $_POST['post_kelurahan'];
		$nohp = $_POST['post_nohp'];
		$norek = $_POST['post_norek'];

		$data_insert = array(
			'imei' => $imei,
			'nama' => $nama,
			'tempat_operator' => $kelurahan,
			'nohp' => $nohp,
			'norekening' => $norek
		);

		$query = $this->operator_m->InsertData('operator',$data_insert);
		if ($query >= 1) {
			//$this->session->set_flashdata('pesan','Tambah Data Sukses');
			redirect('operator');
		} else {
			echo "Insert Data Gagal";
		}
	}

	public function do_delete($id_operator)
	{
		$where = array('id_operator' => $id_operator);
		$query = $this->operator_m->DeleteData('operator',$where);

		if ($query >= 1) {
			//$this->session->set_flashdata('pesan','Hapus Data Sukses');
			redirect('operator');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_operator)
	{
		$query = $this->operator_m->GetData("WHERE id_operator = '$id_operator'");
		$data = array(
			'id_operator' => $query[0]['id_operator'],
			'nama' => $query[0]['nama'],
			'imei' => $query[0]['imei'],
			'tempat_operator' => $query[0]['tempat_operator'],
			'nohp' => $query[0]['nohp'],
			'norekening' => $query[0]['norekening']
		);

		$this->load->view('header');
		$this->load->view('operator_update_v',$data);
        $this->load->view('footer');
	}

	public function do_update()
	{
		$id_operator = $_POST['post_id_operator'];
		$imei = $_POST['post_imei'];
		$nama = $_POST['post_nama'];
		$kelurahan = $_POST['post_kelurahan'];
		$nohp = $_POST['post_nohp'];
		$norek = $_POST['post_norek'];

		$data_update = array(
			'imei' => $imei,
			'nama' => $nama,
			'tempat_operator' => $kelurahan,
			'nohp' => $nohp,
			'norekening' => $norek
		);

		$where = array('id_operator' => $id_operator);
		$query = $this->operator_m->UpdateData('operator',$data_update,$where);
		if ($query >= 1) {
			//$this->session->set_flashdata('pesan','Edit Data Sukses');
			redirect('operator');
		} else {
			echo "Update Data Gagal";
		}
	}
}
