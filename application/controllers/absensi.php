<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {
    
	public function index()
	{
		$date = date('Y-m-d');
		//$date = '2018-12-07';

        $this->load->view('header');
        $data = $this->absensi_m->GetData($date);
		$this->load->view('absensi_v',array('data_absensi' => $data));
		$this->load->view('footer');
	}
}
