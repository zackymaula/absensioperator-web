<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waktu_absen extends CI_Controller {
    
	public function index()
	{
        $this->load->view('header');
        $data = $this->waktu_absen_m->GetData();
		$this->load->view('waktu_absen_v',array('data_waktu_absen' => $data));
        $this->load->view('footer');
	}
	
	public function update()
	{
		$query = $this->waktu_absen_m->GetData();
		$data = array(
			'id_waktu' => $query[0]['id_waktu'],
			'masuk_start' => $query[0]['masuk_start'],
			'masuk_stop' => $query[0]['masuk_stop'],
			'pulang_start' => $query[0]['pulang_start'],
			'pulang_stop' => $query[0]['pulang_stop']
		);

		$this->load->view('header');
		$this->load->view('waktu_absen_update_v',$data);
        $this->load->view('footer');
	}

	public function do_update()
	{
		$id_waktu = $_POST['post_id_waktu'];
		$masuk_start = $_POST['post_masuk_start'];
		$masuk_stop = $_POST['post_masuk_stop'];
		$pulang_start = $_POST['post_pulang_start'];
		$pulang_stop = $_POST['post_pulang_stop'];

		$data_update = array(
			'masuk_start' => $masuk_start,
			'masuk_stop' => $masuk_stop,
			'pulang_start' => $pulang_start,
			'pulang_stop' => $pulang_stop,
		);

		$where = array('id_waktu' => $id_waktu);
		$query = $this->waktu_absen_m->UpdateData('waktu_absen',$data_update,$where);
		if ($query >= 1) {
			//$this->session->set_flashdata('pesan','Edit Data Sukses');
			redirect('waktu_absen');
		} else {
			echo "Update Data Gagal";
		}
	}
}
