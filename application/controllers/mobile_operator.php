<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile_operator extends CI_Controller {

	public function get_operator($imei)
	{
		$query = $this->mobile_operator_m->GetDataOperator("WHERE imei = '$imei'");
		$get_waktu_absen = $this->db->get('waktu_absen')->row();
		//echo json_encode(array('result'=>$query));

		//date('Y-m-d H:i:s')
		$current_time = date('H:i:s');
		$masuk_start = $get_waktu_absen->masuk_start;
		$masuk_stop = $get_waktu_absen->masuk_stop;
		$pulang_start = $get_waktu_absen->pulang_start;
		$pulang_stop = $get_waktu_absen->pulang_stop;

		$date_current = DateTime::createFromFormat('H:i:s', $current_time);
		$date_masuk_start = DateTime::createFromFormat('H:i:s', $masuk_start);
		$date_masuk_stop = DateTime::createFromFormat('H:i:s', $masuk_stop);
		$date_pulang_start = DateTime::createFromFormat('H:i:s', $pulang_start);
		$date_pulang_stop = DateTime::createFromFormat('H:i:s', $pulang_stop);

		$array = array();

		//MASUK
		if ($date_current > $date_masuk_start && $date_current < $date_masuk_stop){
			//$array['result2'][] = array('set_button'=>'1');
			//echo json_encode($array);
			$array[] = array('set_button'=>'masuk');
			echo json_encode(array('result_data_operator'=>$query,
									'result_time_button'=>$array));
		//TELAT
		} else if ($date_current > $date_masuk_stop && $date_current < $date_pulang_start){
			$array[] = array('set_button'=>'telat');
			echo json_encode(array('result_data_operator'=>$query,
									'result_time_button'=>$array));
		//PULANG
		} else if ($date_current > $date_pulang_start && $date_current < $date_pulang_stop){
			$array[] = array('set_button'=>'pulang');
			echo json_encode(array('result_data_operator'=>$query,
									'result_time_button'=>$array));
		//SETELAH PULANG
		} else {
			$array[] = array('set_button'=>'0');
			echo json_encode(array('result_data_operator'=>$query,
									'result_time_button'=>$array));
		}
	}
	
	public function insert_absen()
	{
		$tgl = date('Y-m-d');
		$status_absen_convert;

		$tanggal = date('Y-m-d H:i:s');
		$id_operator = $_POST['id_operator'];
		$nama_operator = $_POST['nama_operator'];
		$tempat_operator = $_POST['tempat_operator'];
		$status_absen = $_POST['status_absen'];
		$keterangan = $_POST['keterangan'];

		if ($status_absen == "M") {
			$status_absen_convert = "M";
		} else if ($status_absen == "MT"){
			$status_absen_convert = "M";
		} else if ($status_absen == "P"){
			$status_absen_convert = "P";
		}

		$data_insert = array(
			'tanggal' => $tanggal,
			'id_operator' => $id_operator,
			'nama_operator' => $nama_operator,
			'tempat_operator' => $tempat_operator,
			'status_absen' => $status_absen,
			'keterangan' => $keterangan
			);

		$cek_log = $this->cek_log($id_operator,$status_absen_convert,$tgl);
		if ($cek_log == 0) {
			$query = $this->mobile_operator_m->InsertData('log_absensi',$data_insert);
		}

		echo $cek_log;
	}

	public function cek_log($id_operator,$status_absen,$tanggal)
	{
		$query = $this->mobile_operator_m->CekLogAbsensi($id_operator,$status_absen,$tanggal);

		$return = 0;
		foreach ($query as $data) {
			$return = 1;//json_encode(array('result'=>$query));
		}

		return $return;
	}
}