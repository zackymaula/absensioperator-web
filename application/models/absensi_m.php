<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi_m extends CI_Model {

	public function GetData($where="")
	{
		$data = $this->db->query("SELECT o.id_operator, o.nama, o.tempat_operator, DATE_FORMAT(m.tanggal, '%Y-%m-%d') AS tanggal_m, DATE_FORMAT(p.tanggal, '%Y-%m-%d') AS tanggal_p, m.status_absen AS status_m, p.status_absen AS status_p
									FROM operator AS o
									LEFT JOIN (
										SELECT * FROM log_absensi
										WHERE status_absen LIKE 'M%'
										AND tanggal LIKE '".$where."%'
									) AS m
									ON o.id_operator = m.id_operator
									LEFT JOIN (
										SELECT * FROM log_absensi
										WHERE status_absen = 'P'
										AND tanggal LIKE '".$where."%'
									) AS p
									ON o.id_operator = p.id_operator
									ORDER BY o.nama ASC"); // '2018-12-07'
		return $data->result_array();
	}

	public function InsertData($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	public function UpdateData($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}

	public function DeleteData($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}
}
