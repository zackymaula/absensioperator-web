<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile_operator_m extends CI_Model {
	
	public function GetDataOperator($where="")
	{
		$data = $this->db->query('SELECT * FROM operator '.$where);
		return $data->result_array();
	}

	public function InsertData($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	public function CekLogAbsensi($id_operator="",$status_absen="",$tanggal="")
	{
		$data = $this->db->query("SELECT * FROM log_absensi
									WHERE id_operator = '".$id_operator."'
									AND status_absen LIKE '".$status_absen."%'
									AND tanggal LIKE '".$tanggal."%'");
		return $data->result_array();
	}
}
