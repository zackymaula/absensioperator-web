
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- TABLE STRIPED -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Operator</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="<?php echo base_url() ?>operator/insert" type="button" class="btn btn-primary btn-block"><i class="fa fa-plus-square"></i> Tambah</a>
                            </div>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kelurahan</th>
                                <th>Nama</th>
                                <th>No HP</th>
                                <th>IMEI</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 1;
                            foreach ($data_operator as $data) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $data['tempat_operator']; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo $data['nohp']; ?></td>
                                <td><?php echo $data['imei']; ?></td>
                                <td>
                                    <a href="<?php echo base_url().'operator/update/'.$data['id_operator']; ?>" type="button" class="btn btn-warning btn-xs">Edit</a>
                                    <a href="<?php echo base_url().'operator/do_delete/'.$data['id_operator']; ?>" type="button" class="btn btn-danger btn-xs">Delete</a>
                                </td>
                            </tr>
                            <?php $i++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END TABLE STRIPED -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
		