
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Tambah Operator</h3>
            </div>
            <div class="panel-body">
            <form class="form-auth-small" method="POST" action="<?php echo base_url() ?>operator/do_insert">
                <div class="row">
                    <div class="col-md-2"><h5>IMEI</h5></div>
                    <div class="col-md-10"><input type="text" name="post_imei" class="form-control" placeholder="IMEI" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>Nama</h5></div>
                    <div class="col-md-10"><input type="text" name="post_nama" class="form-control" placeholder="Nama" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>Lokasi Kelurahan</h5></div>
                    <div class="col-md-10"><input type="text" name="post_kelurahan" class="form-control" placeholder="Lokasi Kelurahan" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>No Handphone</h5></div>
                    <div class="col-md-10"><input type="text" name="post_nohp" class="form-control" placeholder="No Handphone" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>No Rekening</h5></div>
                    <div class="col-md-10"><input type="text" name="post_norek" class="form-control" placeholder="No Rekening" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary btn-block">SIMPAN</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
		