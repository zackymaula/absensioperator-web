
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- TABLE STRIPED -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Waktu Absen</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Masuk Start</th>
                                <th>Masuk Stop</th>
                                <th>Pulang Start</th>
                                <th>Pulang Stop</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($data_waktu_absen as $data) {
                            ?>
                            <tr>
                                <td><?php echo $data['masuk_start']; ?></td>
                                <td><?php echo $data['masuk_stop']; ?></td>
                                <td><?php echo $data['pulang_start']; ?></td>
                                <td><?php echo $data['pulang_stop']; ?></td>
                                <td><a href="<?php echo base_url()?>waktu_absen/update" type="button" class="btn btn-warning btn-xs">Edit</a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END TABLE STRIPED -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
		