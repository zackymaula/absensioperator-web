
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- TABLE STRIPED -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Waktu Absen</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Masuk Start</th>
                                <th>Masuk Stop</th>
                                <th>Pulang Start</th>
                                <th>Pulang Stop</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form class="form-auth-small" method="POST" action="<?php echo base_url() ?>waktu_absen/do_update">
                            <input type="hidden" name="post_id_waktu" value="<?php echo $id_waktu; ?>" class="form-control" placeholder="id_operator" required>
                            <tr>
                                <td><input type="text" name="post_masuk_start" value="<?php echo $masuk_start; ?>" class="form-control" required></td>
                                <td><input type="text" name="post_masuk_stop" value="<?php echo $masuk_stop; ?>" class="form-control" required></td>
                                <td><input type="text" name="post_pulang_start" value="<?php echo $pulang_start; ?>" class="form-control" required></td>
                                <td><input type="text" name="post_pulang_stop" value="<?php echo $pulang_stop; ?>" class="form-control" required></td>
                                <td><button type="submit" class="btn btn-primary btn-xs">Simpan</button></td>
                            </tr>
                            </form>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END TABLE STRIPED -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
		