
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- TABLE STRIPED -->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Absensi | <?php echo date('D, d M Y'); ?></h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped">
						<thead>
                            <tr>
								<th>No</th>
                                <th>Nama</th>
                                <th>Tempat Operator</th>
                                <th>Masuk</th>
                                <th>Pulang</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php 
							$i = 1;
                            foreach ($data_absensi as $data) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo $data['tempat_operator']; ?></td>
								<td>
									<?php
										if ($data['status_m'] == 'M') {
											echo "<span class='label label-success'>MASUK</span>";
										} else if ($data['status_m'] == 'MT'){
											echo "<span class='label label-warning'>MASUK TELAT</span>";
										} else {
											echo "<span class='label label-danger'>BELUM ABSEN</span>";
										}
									?>
								</td>
								<td>
									<?php
										if ($data['status_p'] == 'P') {
											echo "<span class='label label-success'>PULANG</span>";
										} else {
											echo "<span class='label label-danger'>BELUM ABSEN</span>";
										}
									?>
								</td>
                            </tr>
                            <?php $i++; } ?>
                        </tbody>
					</table>
				</div>
			</div>
			<!-- END TABLE STRIPED -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
		