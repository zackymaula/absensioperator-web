
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Operator</h3>
            </div>
            <div class="panel-body">
            <form class="form-auth-small" method="POST" action="<?php echo base_url() ?>operator/do_update">
                <input type="hidden" name="post_id_operator" value="<?php echo $id_operator; ?>" class="form-control" placeholder="id_operator" required>
                <div class="row">
                    <div class="col-md-2"><h5>IMEI</h5></div>
                    <div class="col-md-10"><input type="text" name="post_imei" value="<?php echo $imei; ?>" class="form-control" placeholder="IMEI" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>Nama</h5></div>
                    <div class="col-md-10"><input type="text" name="post_nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>Lokasi Kelurahan</h5></div>
                    <div class="col-md-10"><input type="text" name="post_kelurahan" value="<?php echo $tempat_operator; ?>" class="form-control" placeholder="Lokasi Kelurahan" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>No Handphone</h5></div>
                    <div class="col-md-10"><input type="text" name="post_nohp" value="<?php echo $nohp; ?>" class="form-control" placeholder="No Handphone" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"><h5>No Rekening</h5></div>
                    <div class="col-md-10"><input type="text" name="post_norek" value="<?php echo $norekening; ?>" class="form-control" placeholder="No Rekening" required></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary btn-block">EDIT</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
		